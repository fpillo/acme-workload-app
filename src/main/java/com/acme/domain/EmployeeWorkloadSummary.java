package com.acme.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = {"pis"})
@ToString(of = {"pis", "balance"})
public class EmployeeWorkloadSummary {

    private String pis;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration balance;

    private List<DayBalance> history;

    public EmployeeWorkloadSummary(final String pis) {
        this.pis = pis;
        this.balance = Duration.ZERO;
        this.history = new ArrayList<>();
    }

    public void addHistory(final DayBalance dayBalance) {
        history.add(dayBalance);
        balance = balance.plus(dayBalance.getBalance().toMinutes(), ChronoUnit.MINUTES);
    }

}
