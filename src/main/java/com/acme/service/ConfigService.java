package com.acme.service;

import com.acme.domain.Config;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class ConfigService {

    private final ObjectMapper mapper;

    public ConfigService(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public Config load(final String fileName) throws IOException {
        final File file = new File(fileName);
        final Config config = mapper.readValue(file, Config.class);

        return config;
    }

}
