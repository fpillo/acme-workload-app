package com.acme.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = {"pis"})
@ToString(of = {"pis", "name"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {

    @JsonProperty(value = "pis_number")
    private String pis;

    private String name;

    @JsonProperty(value = "workload")
    private List<EmployeeWorkload> workloads;

    public EmployeeWorkload getFirst() {
        return workloads.get(0);
    }

}
