package com.acme.domain;

import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Getter
@ToString
public class WorkDay {

    private final String pis;

    private final LocalDate date;

    private final Long workMinutes;

    private final Long restMinutes;

    public WorkDay(final String pis, final LocalDate date, final LocalTime start, final LocalTime end) {
        this.pis = pis;
        this.date = date;
        this.workMinutes = createWorkMinutes(start, end);
        this.restMinutes = 0l;
    }

    public WorkDay(final String pis, final LocalDate date, final LocalTime start, final LocalTime end, final LocalTime startRest, final LocalTime endRest) {
        this.pis = pis;
        this.date = date;
        this.workMinutes = createWorkMinutes(start, end);
        this.restMinutes = createRestMinutes(startRest, endRest);
    }

    private Long createWorkMinutes(final LocalTime start, final LocalTime end) {
        return start.until(end, ChronoUnit.MINUTES);
    }

    private Long createRestMinutes(final LocalTime startRest, final LocalTime endRest) {
        return startRest.until(endRest, ChronoUnit.MINUTES);
    }

}
