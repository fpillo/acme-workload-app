package com.acme.service;

import com.acme.domain.EmployeeEntry;
import com.acme.domain.TimeClock;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TimeClockService {

    private final ObjectMapper mapper;

    public TimeClockService(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public TimeClock load(final String fileName) throws IOException {
        final File file = new File(fileName);
        final List<EmployeeEntry> entries = mapper.readValue(file, new TypeReference<List<EmployeeEntry>>() {
        });

        return new TimeClock(entries);
    }

}
