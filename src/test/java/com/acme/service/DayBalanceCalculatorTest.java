package com.acme.service;

import com.acme.domain.WorkDay;
import com.acme.domain.EmployeeWorkload;
import com.acme.domain.DayBalance;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;

public class DayBalanceCalculatorTest {

    private DayBalanceCalculator calculator = new DayBalanceCalculator();

    @Test
    public void test_calculate_regularWorkLoad_regularInterval_noOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.ZERO, dayBalance.getBalance());
    }

    @Test
    public void test_calculate_exceededWorkLoad_regularInterval_addOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(7, 10, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.of(50, ChronoUnit.MINUTES), dayBalance.getBalance());
    }

    @Test
    public void test_calculate_irregularWorkLoad_regularInterval_minusOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(16, 10, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.of(-50, ChronoUnit.MINUTES), dayBalance.getBalance());
    }

    @Test
    public void test_calculate_regularWorkload_exceededInterval_noOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(14, 0, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.ZERO, dayBalance.getBalance());
    }

    @Test
    public void test_calculate_exceededWorkload_exceededInterval_addOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(6, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(14, 0, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.of(120, ChronoUnit.MINUTES), dayBalance.getBalance());
    }

    @Test
    public void test_calculate_irregularWorkload_exceededInterval_minusOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(9, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(14, 0, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.of(-60, ChronoUnit.MINUTES), dayBalance.getBalance());
    }

    @Test
    public void test_calculate_regularWorkload_irregularInterval_addOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(12, 30, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.of(60, ChronoUnit.MINUTES), dayBalance.getBalance());
    }

    @Test
    public void test_calculate_irregularWorkloadEqual50Percent_irregularInterval_addOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(9, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(12, 30, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.ZERO, dayBalance.getBalance());
    }

    @Test
    public void test_calculate_irregularWorkloadBigger50Percent_irregularInterval_addOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(10, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(12, 10, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.of(-20, ChronoUnit.MINUTES), dayBalance.getBalance());
    }

    @Test
    public void test_calculate_irregularWorkloadLess50Percent_irregularInterval_addOverload() {
        final EmployeeWorkload workload = new EmployeeWorkload(480L, 60L);
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(11, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(12, 20, 0)
        );

        final DayBalance dayBalance = calculator.calculate(workload, monday);

        assertEquals(LocalDate.of(2018, 5, 7), dayBalance.getDay());
        assertEquals(Duration.of(-100, ChronoUnit.MINUTES), dayBalance.getBalance());
    }


}
