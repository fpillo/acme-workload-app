package com.acme.service;

import com.acme.domain.WorkDay;
import com.acme.domain.Employee;
import com.acme.domain.EmployeeWorkload;
import com.acme.domain.DayBalance;
import com.acme.domain.EmployeeWorkloadSummary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class SummaryCalculatorTest {

    @InjectMocks
    private SummaryCalculator summaryCalculator;

    @Mock
    private DayBalanceCalculator dayBalanceCalculator;

    @Test
    public void test_calculate_oneDay() {
        final Employee employee = new Employee("12345", "name", asList(new EmployeeWorkload(480l, 60l)));
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final DayBalance dayBalance = new DayBalance(LocalDate.of(2018, 5, 7));
        dayBalance.addBalance(0);
        Mockito.when(dayBalanceCalculator.calculate(employee.getFirst(), monday)).thenReturn(dayBalance);

        final EmployeeWorkloadSummary summary = summaryCalculator.calculate(employee, asList(monday));

        assertEquals("12345", summary.getPis());
        assertEquals(Duration.ZERO, summary.getBalance());
        assertEquals(1, summary.getHistory().size());
    }

    @Test
    public void test_calculate_twoDays() {
        final Employee employee = new Employee("12345", "name", asList(new EmployeeWorkload(480l, 60l)));
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final WorkDay tuesday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final DayBalance mondayBalance = new DayBalance(LocalDate.of(2018, 5, 7));
        mondayBalance.addBalance(100);

        final DayBalance tuesdayBalance = new DayBalance(LocalDate.of(2018, 5, 8));
        tuesdayBalance.addBalance(100);

        Mockito.when(dayBalanceCalculator.calculate(employee.getFirst(), monday)).thenReturn(mondayBalance);
        Mockito.when(dayBalanceCalculator.calculate(employee.getFirst(), tuesday)).thenReturn(tuesdayBalance);

        final EmployeeWorkloadSummary summary = summaryCalculator.calculate(employee, asList(monday, tuesday));

        assertEquals("12345", summary.getPis());
        assertEquals(Duration.of(200, ChronoUnit.MINUTES), summary.getBalance());
        assertEquals(2, summary.getHistory().size());
    }

    @Test
    public void test_calculate_threeDays() {
        final Employee employee = new Employee("12345", "name", asList(new EmployeeWorkload(480l, 60l)));
        final WorkDay monday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final WorkDay tuesday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final WorkDay wednesday = new WorkDay("12345",
                LocalDate.of(2018, 5, 7),
                LocalTime.of(8, 0, 0),
                LocalTime.of(17, 0, 0),
                LocalTime.of(12, 0, 0),
                LocalTime.of(13, 0, 0)
        );

        final DayBalance mondayBalance = new DayBalance(LocalDate.of(2018, 5, 7));
        mondayBalance.addBalance(100);

        final DayBalance tuesdayBalance = new DayBalance(LocalDate.of(2018, 5, 8));
        tuesdayBalance.addBalance(0);

        final DayBalance wednesdayBalance = new DayBalance(LocalDate.of(2018, 5, 9));
        wednesdayBalance.addBalance(-100);

        Mockito.when(dayBalanceCalculator.calculate(employee.getFirst(), monday)).thenReturn(mondayBalance);
        Mockito.when(dayBalanceCalculator.calculate(employee.getFirst(), tuesday)).thenReturn(tuesdayBalance);
        Mockito.when(dayBalanceCalculator.calculate(employee.getFirst(), wednesday)).thenReturn(wednesdayBalance);

        final EmployeeWorkloadSummary summary = summaryCalculator.calculate(employee, asList(monday, tuesday, wednesday));

        assertEquals("12345", summary.getPis());
        assertEquals(Duration.ZERO, summary.getBalance());
        assertEquals(3, summary.getHistory().size());
    }

}
