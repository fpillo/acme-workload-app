package com.acme.service;

import com.acme.domain.WorkDay;
import com.acme.domain.EmployeeWorkload;
import com.acme.domain.DayBalance;

public class DayBalanceCalculator {

    public DayBalance calculate(final EmployeeWorkload employeeWorkload, final WorkDay workDay) {
        final DayBalance dayBalance = new DayBalance(workDay.getDate());
        dayBalance.addBalance(workDay.getWorkMinutes());
        dayBalance.minusBalance(selectRest(workDay, employeeWorkload));

        if (workDay.getRestMinutes() < employeeWorkload.getTotalRestIntervalMinutes()) {
            float percentage = dayBalance.getBalance().toMinutes() * 100 / employeeWorkload.getTotalWorkloadMinutes();
            if (percentage >= 50.0) {
                final Long restBalance = employeeWorkload.getTotalRestIntervalMinutes() - workDay.getRestMinutes();
                dayBalance.addBalance(restBalance);
            }
        }
        dayBalance.minusBalance(employeeWorkload.getTotalWorkloadMinutes());

        return dayBalance;
    }

    private Long selectRest(final WorkDay workDay, final EmployeeWorkload employeeWorkload) {
        return workDay.getRestMinutes() <= employeeWorkload.getTotalRestIntervalMinutes() ? workDay.getRestMinutes() : employeeWorkload.getTotalRestIntervalMinutes();
    }
}
