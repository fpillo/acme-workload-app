package com.acme.service;

import com.acme.domain.WorkDay;
import com.acme.domain.EmployeeEntry;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WorkDayCalculator {

    public List<WorkDay> calculateMinutesByDay(final EmployeeEntry employeeEntry) {
        final String pis = employeeEntry.getPis();
        final Map<LocalDate, List<LocalDateTime>> groupByDate = groupEntryByDay(employeeEntry);

        return calculate(pis, groupByDate);
    }

    private final Map<LocalDate, List<LocalDateTime>> groupEntryByDay(final EmployeeEntry employeeEntry) {
        final Map<LocalDate, List<LocalDateTime>> groupByDay = new LinkedHashMap<>();
        final List<LocalDateTime> entries = employeeEntry.getEntries();
        Collections.sort(entries);

        entries.forEach(localDateTime -> {
            final LocalDate localDate = localDateTime.toLocalDate();
            if (!groupByDay.containsKey(localDate)) {
                groupByDay.put(localDate, new ArrayList<>());
            }
            groupByDay.get(localDate).add(localDateTime);
        });

        return groupByDay;
    }

    private List<WorkDay> calculate(final String pis, final Map<LocalDate, List<LocalDateTime>> groupByDay) {
        final List<WorkDay> workDay = new ArrayList<>();

        groupByDay.forEach(((localDate, localDateTimes) -> workDay.add(calculateMinutesByDay(pis, localDate, localDateTimes))));

        return workDay;
    }

    private WorkDay calculateMinutesByDay(final String pis, final LocalDate localDate, final List<LocalDateTime> times) {
        final LocalTime start = times.get(0).toLocalTime();
        final LocalTime end = times.get((times.size() - 1)).toLocalTime();

        if (times.size() == 4) {
            final LocalTime startRest = times.get(1).toLocalTime();
            final LocalTime endRest = times.get(2).toLocalTime();

            return new WorkDay(pis, localDate, start, end, startRest, endRest);
        }

        return new WorkDay(pis, localDate, start, end);
    }

}
