# ACME Workload app

Obs: Por se tratar de uma aplicação demonstrativa não foram utilizados tratamentos de erros, apenas técnicas/boa práticas de desenvolvimento, testes de unidades.

* os campos de input "day" não foram utilizados por não estarem nas regras de negócio da aplicação. Caso necessário podemos refatorar a aplicação com as regras propostas para esse campo.

## Getting Started

## Requisitos

* JDK 8+ [Linux](http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html)
* Maven 3+ [Install](https://maven.apache.org/install.html)

## Comandos

### Run test

```
$ mvn test
```

## Package

```
$ mvn package
```

## Run

Colocar os arquivos de configuração (config.json) e de entrada de horários (timeclock_entries.json) dentro da pasta target criada pelo build do maven (ou na mesma pasta que contenha o jar da aplicação).

Ainda dentro da pasta target

```
$ java -jar app-1.0-SNAPSHOT-jar-with-dependencies.jar config.json timeclock_entries.json
```
