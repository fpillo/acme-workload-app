package com.acme.service;

import com.acme.domain.WorkDay;
import com.acme.domain.EmployeeEntry;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class WorkDayCalculatorTest {

    private WorkDayCalculator workDayCalculator = new WorkDayCalculator();

    @Test
    public void test_calculateByDay_oneDayEntry_noRestInterval() {
        final LocalDateTime start = LocalDateTime.of(2018, 5, 7, 8, 0, 0);
        final LocalDateTime end = LocalDateTime.of(2018, 5, 7, 17, 0, 0);

        final EmployeeEntry employeeEntry = new EmployeeEntry("12345", Arrays.asList(start, end));

        final List<WorkDay> workDays = workDayCalculator.calculateMinutesByDay(employeeEntry);

        Assert.assertEquals(1, workDays.size());
        Assert.assertEquals("12345", workDays.get(0).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 7), workDays.get(0).getDate());
        Assert.assertEquals(new Long(0), workDays.get(0).getRestMinutes());
        Assert.assertEquals(new Long(540), workDays.get(0).getWorkMinutes());
    }

    @Test
    public void test_calculateByDay_oneDayEntry_withRestInterval() {
        final LocalDateTime start = LocalDateTime.of(2018, 5, 7, 8, 0, 0);
        final LocalDateTime startInterval = LocalDateTime.of(2018, 5, 7, 13, 0, 0);
        final LocalDateTime endInterval = LocalDateTime.of(2018, 5, 7, 14, 0, 0);
        final LocalDateTime end = LocalDateTime.of(2018, 5, 7, 17, 0, 0);

        final EmployeeEntry employeeEntry = new EmployeeEntry("12345", Arrays.asList(start, startInterval, endInterval, end));

        final List<WorkDay> workDays = workDayCalculator.calculateMinutesByDay(employeeEntry);

        Assert.assertEquals(1, workDays.size());
        Assert.assertEquals("12345", workDays.get(0).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 7), workDays.get(0).getDate());
        Assert.assertEquals(new Long(60), workDays.get(0).getRestMinutes());
        Assert.assertEquals(new Long(540), workDays.get(0).getWorkMinutes());
    }

    @Test
    public void test_calculateByDay_twoDayEntry_noRestInterval() {
        final LocalDateTime start1 = LocalDateTime.of(2018, 5, 7, 8, 0, 0);
        final LocalDateTime end1 = LocalDateTime.of(2018, 5, 7, 17, 0, 0);

        final LocalDateTime start2 = LocalDateTime.of(2018, 5, 8, 8, 0, 0);
        final LocalDateTime end2 = LocalDateTime.of(2018, 5, 8, 20, 0, 0);

        final EmployeeEntry employeeEntry = new EmployeeEntry("12345", Arrays.asList(start1, end1, start2, end2));

        final List<WorkDay> workDays = workDayCalculator.calculateMinutesByDay(employeeEntry);

        Assert.assertEquals(2, workDays.size());

        Assert.assertEquals("12345", workDays.get(0).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 7), workDays.get(0).getDate());
        Assert.assertEquals(new Long(0), workDays.get(0).getRestMinutes());
        Assert.assertEquals(new Long(540), workDays.get(0).getWorkMinutes());

        Assert.assertEquals("12345", workDays.get(1).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 8), workDays.get(1).getDate());
        Assert.assertEquals(new Long(0), workDays.get(1).getRestMinutes());
        Assert.assertEquals(new Long(720), workDays.get(1).getWorkMinutes());
    }

    @Test
    public void test_calculateByDay_twoDayEntry_withRestInterval() {
        final LocalDateTime start1 = LocalDateTime.of(2018, 5, 7, 8, 0, 0);
        final LocalDateTime startInterval1 = LocalDateTime.of(2018, 5, 7, 13, 0, 0);
        final LocalDateTime endInterval1 = LocalDateTime.of(2018, 5, 7, 14, 0, 0);
        final LocalDateTime end1 = LocalDateTime.of(2018, 5, 7, 17, 0, 0);

        final LocalDateTime start2 = LocalDateTime.of(2018, 5, 8, 9, 0, 0);
        final LocalDateTime startInterval2 = LocalDateTime.of(2018, 5, 8, 13, 0, 0);
        final LocalDateTime endInterval2 = LocalDateTime.of(2018, 5, 8, 15, 0, 0);
        final LocalDateTime end2 = LocalDateTime.of(2018, 5, 8, 20, 0, 0);

        final EmployeeEntry employeeEntry = new EmployeeEntry("12345", Arrays.asList(start1, startInterval1, endInterval1, end1, start2, startInterval2, endInterval2, end2));

        final List<WorkDay> workDays = workDayCalculator.calculateMinutesByDay(employeeEntry);

        Assert.assertEquals(2, workDays.size());

        Assert.assertEquals("12345", workDays.get(0).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 7), workDays.get(0).getDate());
        Assert.assertEquals(new Long(60), workDays.get(0).getRestMinutes());
        Assert.assertEquals(new Long(540), workDays.get(0).getWorkMinutes());

        Assert.assertEquals("12345", workDays.get(1).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 8), workDays.get(1).getDate());
        Assert.assertEquals(new Long(120), workDays.get(1).getRestMinutes());
        Assert.assertEquals(new Long(660), workDays.get(1).getWorkMinutes());
    }

    @Test
    public void test_calculateByDay_twoDayEntry_mixedRestInterval() {
        final LocalDateTime start1 = LocalDateTime.of(2018, 5, 7, 8, 0, 0);
        final LocalDateTime startInterval1 = LocalDateTime.of(2018, 5, 7, 13, 0, 0);
        final LocalDateTime endInterval1 = LocalDateTime.of(2018, 5, 7, 14, 0, 0);
        final LocalDateTime end1 = LocalDateTime.of(2018, 5, 7, 17, 0, 0);

        final LocalDateTime start2 = LocalDateTime.of(2018, 5, 8, 9, 0, 0);
        final LocalDateTime end2 = LocalDateTime.of(2018, 5, 8, 20, 0, 0);

        final EmployeeEntry employeeEntry = new EmployeeEntry("12345", Arrays.asList(start1, startInterval1, endInterval1, end1, start2, end2));

        final List<WorkDay> workDays = workDayCalculator.calculateMinutesByDay(employeeEntry);

        Assert.assertEquals(2, workDays.size());

        Assert.assertEquals("12345", workDays.get(0).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 7), workDays.get(0).getDate());
        Assert.assertEquals(new Long(60), workDays.get(0).getRestMinutes());
        Assert.assertEquals(new Long(540), workDays.get(0).getWorkMinutes());

        Assert.assertEquals("12345", workDays.get(1).getPis());
        Assert.assertEquals(LocalDate.of(2018, 5, 8), workDays.get(1).getDate());
        Assert.assertEquals(new Long(0), workDays.get(1).getRestMinutes());
        Assert.assertEquals(new Long(660), workDays.get(1).getWorkMinutes());
    }

}
