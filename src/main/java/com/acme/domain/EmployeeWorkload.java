package com.acme.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeWorkload {

    @JsonProperty(value = "workload_in_minutes")
    private Long totalWorkloadMinutes;

    @JsonProperty(value = "minimum_rest_interval_in_minutes")
    private Long totalRestIntervalMinutes;

}
