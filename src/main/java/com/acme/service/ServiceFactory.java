package com.acme.service;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ServiceFactory {

    public static WorkloadService createWorkloadService() {
        final DayBalanceCalculator dayBalanceCalculator = new DayBalanceCalculator();
        final SummaryCalculator summaryCalculator = new SummaryCalculator(dayBalanceCalculator);
        final WorkDayCalculator workDayCalculator = new WorkDayCalculator();

        return new WorkloadService(summaryCalculator, workDayCalculator);
    }

    public static ConfigService getConfigService(final ObjectMapper mapper) {
        return new ConfigService(mapper);
    }

    public static TimeClockService getTimeClockService(final ObjectMapper mapper) {
        return new TimeClockService(mapper);
    }

}
