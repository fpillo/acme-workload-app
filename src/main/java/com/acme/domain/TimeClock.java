package com.acme.domain;

import lombok.Getter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Getter
public class TimeClock {

    private final Set<EmployeeEntry> employeeEntries;

    public TimeClock(final Collection<EmployeeEntry> entries) {
        this.employeeEntries = new HashSet<>();
        this.employeeEntries.addAll(entries);
    }

    public Optional<EmployeeEntry> findEntryByPis(final String pis) {
        return employeeEntries.stream().filter(entry -> entry.getPis().equals(pis)).findFirst();
    }

}
