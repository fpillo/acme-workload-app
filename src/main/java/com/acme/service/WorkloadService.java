package com.acme.service;

import com.acme.domain.WorkDay;
import com.acme.domain.Config;
import com.acme.domain.Employee;
import com.acme.domain.EmployeeEntry;
import com.acme.domain.TimeClock;
import com.acme.domain.EmployeeWorkloadSummary;

import java.util.ArrayList;
import java.util.List;

public class WorkloadService {

    private final SummaryCalculator summaryCalculator;

    private final WorkDayCalculator workDayCalculator;

    public WorkloadService(final SummaryCalculator summaryCalculator, final WorkDayCalculator workDayCalculator) {
        this.summaryCalculator = summaryCalculator;
        this.workDayCalculator = workDayCalculator;
    }

    public List<EmployeeWorkloadSummary> getWorkload(final Config config, final TimeClock timeClock) {
        final List<EmployeeWorkloadSummary> summaries = new ArrayList<>();

        for (final Employee employee : config.getEmployees()) {
            timeClock
                    .findEntryByPis(employee.getPis())
                    .ifPresent(entry -> summaries.add(getByEmployee(employee, entry))
            );

        }

        return summaries;
    }

    private EmployeeWorkloadSummary getByEmployee(final Employee employee, final EmployeeEntry employeeEntry) {
        final List<WorkDay> workDays = workDayCalculator.calculateMinutesByDay(employeeEntry);
        return summaryCalculator.calculate(employee, workDays);
    }

}
