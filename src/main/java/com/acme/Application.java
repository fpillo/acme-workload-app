package com.acme;

import com.acme.domain.EmployeeWorkloadSummary;
import com.acme.service.ConfigService;
import com.acme.service.ServiceFactory;
import com.acme.service.TimeClockService;
import com.acme.service.WorkloadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.util.List;

public class Application {

    public static void main(final String[] args) throws Exception {
        final ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

        final WorkloadService service = ServiceFactory.createWorkloadService();
        final ConfigService configService = ServiceFactory.getConfigService(mapper);
        final TimeClockService timeClockService = ServiceFactory.getTimeClockService(mapper);

        final List<EmployeeWorkloadSummary> summaries = service.getWorkload(
                configService.load(getConfigFileName(args)),
                timeClockService.load(getTimeClockFileName(args))
        );

        printResult(summaries, mapper);
    }

    private static void printResult(final List<EmployeeWorkloadSummary> summaries, final ObjectMapper mapper) throws JsonProcessingException {
        System.out.print(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(summaries));
    }

    private static String getConfigFileName(final String[] args) {
        return args[0];
    }

    private static String getTimeClockFileName(final String[] args) {
        return args[1];
    }

}
