package com.acme.service;

import com.acme.domain.WorkDay;
import com.acme.domain.Employee;
import com.acme.domain.DayBalance;
import com.acme.domain.EmployeeWorkloadSummary;

import java.util.List;

public class SummaryCalculator {

    private final DayBalanceCalculator dayBalanceCalculator;

    public SummaryCalculator(final DayBalanceCalculator dayBalanceCalculator) {
        this.dayBalanceCalculator = dayBalanceCalculator;
    }

    public EmployeeWorkloadSummary calculate(final Employee employee, final List<WorkDay> workDays) {
        final EmployeeWorkloadSummary summary = new EmployeeWorkloadSummary(employee.getPis());

        workDays.forEach(workDay -> {
            final DayBalance dayBalance = dayBalanceCalculator.calculate(employee.getFirst(), workDay);
            summary.addHistory(dayBalance);
        });

        return summary;
    }

}
