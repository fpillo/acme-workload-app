package com.acme.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Getter
@EqualsAndHashCode(of = {"day"})
@ToString
public class DayBalance {

    private LocalDate day;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration balance;

    public DayBalance(final LocalDate day) {
        this.day = day;
        this.balance = Duration.ZERO;
    }

    public void addBalance(final long minutes) {
        balance = balance.plus(minutes, ChronoUnit.MINUTES);
    }

    public void minusBalance(final long minutes) {
        balance = balance.minus(minutes, ChronoUnit.MINUTES);
    }

}
